import {USPS} from '../model/usps/usps';
import {Address} from "../addressValidator/address";
import {log} from '../log'
import {AxiosPromise} from "axios";

/**
 * @Route('/api/addressValidation')
 * @param addresses
 */
module.exports.addressValidation = (addresses: Address[]): Promise<AxiosPromise<any>> => {
    const usps = new USPS()

    return usps.validateAddress(addresses)
        .then((result) => result)
        .catch(err => { log.info(err.message, ' addressValidation') })
}

