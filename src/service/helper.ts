import {Address} from "../addressValidator/address";
import {log} from "../log";

export module Helper {
    /**
     * Function to attach id to address model
     * @param addresses
     * @private
     */
    export function _attachId(addresses: Address[]): Address[] {
            try{
                for(let i=0; i<addresses.length; i++) {
                    addresses[i].id = i
                }
            } catch (err) {
                log.info(err.message)
            }
            return addresses
    }

    /**
     * Function to chunk array
     * @param array
     * @param size
     */
    export function chunk(array :Address[], size: number) :Address[][] {
            const chunked_arr = [];
            let index = 0;
            while (index < array.length) {
                chunked_arr.push(array.slice(index, size + index));
                index += size;
            }
            return chunked_arr;
        }
}