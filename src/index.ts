require('dotenv/config')

const port = process.env.PORT || 5555;

const server = require('./app')

server.listen(port, () => {
    console.info(`Server running on port ${port}`)
    //log.info(`Server running on port ${port}`)
});
