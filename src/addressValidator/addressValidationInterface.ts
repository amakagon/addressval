import {RequestInterface} from "./requestInterface";
import {AxiosPromise} from "axios";

export interface AddressValidationInterface {
    addressValidation(item: RequestInterface): AxiosPromise
}