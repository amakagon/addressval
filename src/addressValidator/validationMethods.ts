import {Address} from "./address";

export class ValidationMethods {
    /**
     * Function to put space between digit
     * @param data[]
     */
    parseSplitDigitsArray(data: Address[]): Address[] {
        let res: Address[] = []
        data.forEach((element: any) => {
            res.push(this.parseSplitDigits(element))
        });
        return res
    }

    /**
     * Function to put space between digit
     * @param address
     */
    private parseSplitDigits(address: Address): Address {
        let adrln1 = address.adrln1
        let res=adrln1[0]
        for (let i=1; i < adrln1.length; i++) {
            if(isNaN(adrln1[i-1]) && !isNaN(adrln1[i]) || !isNaN(adrln1[i-1]) && isNaN(adrln1[i])) {
                res += ' '
            }
            res += adrln1[i]
        }
        res = res.replace(/ +(?= )/g,'');
        address.adrln1 = res

        return address
    }

    ignoreAddress2Array(data: Address[]): Address[] {
        let res: Address[] = []
        data.forEach((element: any) => {
            res.push(this.ignoreAddress2(element))
        });
        return res
    }

    private ignoreAddress2(address: Address): Address {
        function hasNumber(myString: string) {
            return /\d/.test(myString);
        }
        if(!hasNumber(address.adrln2)) {
            address.adrln2 = ''
        }

        return address
    }
}