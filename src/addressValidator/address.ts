export interface Address {
    id: number;
    adr_id: number;
    adrnam: string;
    adrln1: any;
    adrln2: any;
    adrln3: any;
    adrcty: string;
    adrstc: string;
    adrpsz: string;
    ctry_name: string;
}