export class ParserStringService {
   private value: string = '';
   private replace: string = '';

    /**
     * Function to parse of each of parse methods
     * @param value
     */
   public parse(value: string): string {
       this.value = value;
       this._isExist()
       ._isIncludeOrderNum()
       ._noValidCharacters()
       ._isNeedToAddApartment();

       return this.value
   }

    /**
     * Function to parse string if it undefined make replace to default value
     * @private
     */
    private _isExist(): ParserStringService {
        if(typeof this.value == "undefined" || !this.value) {
            this.value = this.replace
        }
        return this
    }

    /**
     * Function to replace characters like '#' and & etc..
     * @private
     */
    private _noValidCharacters(): ParserStringService {
        this.value = this.value.replace(/#/g, 'apt ');
        this.value = this.value.replace(/&/g, ' and ');

        return this
    }

    /**
     * Function to replace world include 'Order#' to default value
     * @private
     */
    private _isIncludeOrderNum(): ParserStringService {
        if(this.value.includes('Order#')) {
            this.value = this.replace
        }

        return this
    }
    
    /**
     * Function to replace world include 'Order#' to default value
     * @private
     */
    private _isNeedToAddApartment(): ParserStringService {
        function integer(x: any) {
            if (typeof x !== "number" && typeof x !== "string" || x === "") {
                return NaN;
            } else {
                x = Number(x);
                return x === Math.floor(x) ? x : NaN;
            }
        }

        if(integer(this.value)) {
            this.value = 'apt ' + this.value 
        }

        return this
    }
}
