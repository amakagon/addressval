import { ValidationMethods } from './validationMethods'
import {Address} from "./address";
import {RequestInterface} from "./requestInterface";

export abstract class AddressValidationProvider extends ValidationMethods {
    times: number = 0;

    /**
     * Array of function to validate array of Addresses
     */
    functionNames() {
        return [
            'parseSplitDigitsArray',
            'ignoreAddress2Array',
        ]
    }
    /**
     * Function to call next validate function if that is not exist return false
     */
    next(): string | boolean {
        const name = this.functionNames()[this.times] ? this.functionNames()[this.times] : false;
        name ? this.times++ : '';

        return name
    }

    /**
     * Function to execute value of next validation function if that is not exist return false
     * @param newArray
     */
    getNextRequestData(newArray: any) {
        const validFunc = this.next();

        // @ts-ignore
        return validFunc ? this[validFunc](newArray) : false
    }

    abstract getRequestData(newAddresses: Address[]) : RequestInterface
    abstract separateArrays(newAddresses: Address[], response: any) :[Address[], Address[]]
    abstract parseResponse(data: any): [boolean, object]

}
