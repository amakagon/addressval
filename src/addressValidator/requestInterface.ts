export interface RequestInterface {
    url: string | undefined;
    requestBody: string;
}