const opts = {
    errorEventName:'error',
    logDirectory: __dirname + '/../log', // NOTE: folder must exist and be writable...
    fileNamePattern:'roll-<DATE>.log',
    dateFormat:'YYYY.MM.DD'
};
export const log = require('simple-node-logger').createRollingFileLogger( opts );
