import * as restify from 'restify';
import errors from 'restify-errors'
import {log} from "./log";

const addressValidationRouter = require('./routes/addressValidation.routes')

const server = restify.createServer({
    name: 'tms'
});

server.use(restify.plugins.queryParser({
    mapParams: true
}));

server.use(restify.plugins.bodyParser({
    mapParams: true
}));

server.use(restify.plugins.acceptParser(server.acceptable));

server.pre((req: restify.Request , res: restify.Response, next: restify.Next) => {
    log.info(`${req.method} - ${req.url}`);
    return next();
});

addressValidationRouter.applyRoutes(server);

module.exports = server