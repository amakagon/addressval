import {AddressValidationInterface} from "../../addressValidator/addressValidationInterface";
import axios, {AxiosError, AxiosPromise, AxiosResponse} from "axios";
import {RequestInterface} from "../../addressValidator/requestInterface";
import {log} from "../../log";

class RequestUSPSApi implements AddressValidationInterface{
    /**
     * Get request to USPS API to validate address
     * @param item
     */
    addressValidation(item: RequestInterface): AxiosPromise {
        return axios.get(item.url + item.requestBody, { timeout: 10000 })
            .then((res: AxiosResponse) => {
                return res.data
            })
            .catch((err: AxiosError)  => {
                log.info(err.message, ' getRequest')
            })
    }
}

export const USPSApi = new RequestUSPSApi()