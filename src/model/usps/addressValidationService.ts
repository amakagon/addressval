import {AddressValidationProvider} from "../../addressValidator/addressValidationProvider";
import {ParserStringService} from "../../addressValidator/parserStringService";
import xml2js from 'xml2js'
import {RequestInterface} from "../../addressValidator/requestInterface";
import {Address} from "../../addressValidator/address";

export class AddressValidationService extends AddressValidationProvider  {
    parserStringService: ParserStringService = new ParserStringService();

    /**
     * Return request value
     * @param newAddresses
     */
    getRequestData(newAddresses: Address[]) :RequestInterface {
        const obj = {
            AddressValidateRequest: {
                $: {USERID: process.env.USPS_API_KEY},
                Revision: '1',
                Address: {}
            }
        };
        obj.AddressValidateRequest.Address = this.parseDataArray(newAddresses);
        const requestBody = new xml2js.Builder().buildObject(obj)
        const url = process.env.USPS_URL_PROD;
        return { url, requestBody }
    }

    /**
     * Separate array between valid array and rest of first input data
     * @param newAddresses
     * @param response
     */
    separateArrays(newAddresses: Address[], response: any) :[Address[], Address[]]  {
        let newArrayForRequest: Address[] = []
        const validArray: Address[] = []
        newAddresses.forEach((item: Address) => {
            let [res] = response.filter((elem: any) => item.id == elem['$'].ID)
            if(res.hasOwnProperty('Error')) {
                newArrayForRequest.push(item)
            } else {
                validArray.push(this.parseOutputResponse(res, item))
            }
        })

        return [validArray, newArrayForRequest]
    }

    /**
     * Function to parse response from USPS API
     * @param data
     */
    parseResponse(data: any): [boolean, object] {
        let res: [boolean, object] = [false, {}];
        xml2js.parseString(data, { explicitArray : false }, function (err, result) {
            if(typeof result !== 'undefined' && result.hasOwnProperty('AddressValidateResponse') && result.AddressValidateResponse.hasOwnProperty('Address')) {
                if(!Array.isArray(result.AddressValidateResponse.Address)) {
                    result.AddressValidateResponse.Address = [result.AddressValidateResponse.Address]
                }
                let _status = result.AddressValidateResponse.Address.some((item: any) => item.hasOwnProperty('Error'))
                res = _status ? [false, result.AddressValidateResponse.Address] : [true, result.AddressValidateResponse.Address]
            }
        });

        return res
    }

    /**
     * Function to parse data to call USPS API for each element
     * @param newAddresses
     */
    private parseDataArray(newAddresses: Address[]) :object[] {
        const hVar: object[] = [];
        newAddresses.forEach((element: Address) => {
            hVar.push({
                $: {ID: element.id},
                FirmName: this.parserStringService.parse(element.adrnam),
                Address1: this.parserStringService.parse(element.adrln1) + ' ' + this.parserStringService.parse(element.adrln2) + ' ' + this.parserStringService.parse(element.adrln3),
                Address2: '',
                City: element.adrcty,
                State: element.adrstc,
                Zip5: element.adrpsz,
                Zip4: ''
            })
        });
        return hVar
    }

    /**
     * Function to parse output data
     * @param data
     * @param params
     */
    private parseOutputResponse(data: any, params: Address) :Address {
        return {
            'id': params.id,
            'adr_id': params.adr_id,
            'adrnam': data.FirmName,
            'adrln1': data.Address2,
            'adrln2': data.Address1 ? data.Address1 : '',
            'adrln3': '',
            'adrcty': data.City,
            'adrstc': data.State,
            'adrpsz': data.Zip5,
            'ctry_name': params.ctry_name
        }
    }
}
