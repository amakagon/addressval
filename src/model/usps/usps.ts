import {Address} from "../../addressValidator/address";
import {Helper} from "../../service/helper";
import {USPSApi} from './requestUSPSApi'
import {AddressValidationService} from "./addressValidationService";
import { log } from '../../log'

export class USPS {
    /**
     * Function to validate array of addresses
     * @param addresses
     */
    validateAddress(addresses: Address[]): Promise<any> {
        let chunkArr = Helper.chunk(Helper._attachId(addresses), 5)
        const result: any[] = []

        chunkArr.forEach(item => {
            result.push(this.valAddress(item))
        });

        return Promise.all(result)
            .then(item => Array.prototype.concat.apply([], item))
            .catch(err => { log.info(err.message, ' validateAddress') })
    }

    /**
     * Function to validate chunk of addresses max 5
     * @param addresses
     */
    private async valAddress(addresses: Address[]): Promise<any> {
        const validateAddressService = new AddressValidationService();
        let newAddresses = addresses;
        let breakVar: boolean = false;
        let resultArray: any[] = []

        while (!breakVar) {

            const requestData = validateAddressService.getRequestData(newAddresses)

            await USPSApi.addressValidation(requestData).then((res) => {
                const [status, response] = validateAddressService.parseResponse(res)

                if(!status) {
                    let [validArray, newArrayForRequest] = validateAddressService.separateArrays(newAddresses, response)
                    if(validArray.length > 0) resultArray.push(...validArray)

                    newAddresses = validateAddressService.getNextRequestData(newArrayForRequest);

                    if(!newAddresses) breakVar = true;

                } else {
                    let [validArray] = validateAddressService.separateArrays(newAddresses, response)
                    if(validArray.length > 0) resultArray.push(...validArray)

                    breakVar = true
                }
            }).catch(err => {
                log.info(err.message, ' valAddress')
                breakVar = true
            })
        }
        return resultArray
    }
}