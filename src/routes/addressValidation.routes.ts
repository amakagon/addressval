import {Next, Request, Response} from "restify";
import errors from 'restify-errors'
import {log} from "../log";
const Router = require('restify-router').Router;
const addressValidationRouter = new Router();

const {addressValidation} = require('../controller/controller')


addressValidationRouter.post('/api/addressValidation', (req: Request, res: Response, next: Next) => {
    if(!req.body) {
        return next(new errors.BadRequestError())
    }
    try {
        addressValidation(req.body).then((response: Object)=> {
            let codeMess = Object.values(response).length > 0 ? 'SUCCESS' : 'Error'
            res.send(200, {'code': codeMess, 'data': response});
            return next();
        })
    } catch (error) {
        log.info(error)
        return next(new errors.NotFoundError(error))
    }
});


module.exports = addressValidationRouter;